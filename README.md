**REST** **API**
Umożliwia pobranie współczynników konwersji walut oraz wymianę walut.


1.  Aplikacja jest zdelopyowana pod adresem: `http://exchange-store.herokuapp.com/`


2.  **GET** `"/currencies/{currency}?<filter=>*" `Umożliwia pobranie listy wartość
Przykłady:
    * **GET** `/currencies/PLN` : <long long list>
    * **GET** `/currencies/PLN?filter=USD` => {"rates":{"USD":0.2398976531},"base":"PLN","date":"2020-04-30"}
    * **GET** `/currencies/PLN?filter=USD&filter=EUR` => {"rates":{"EUR":0.2205752603,"USD":0.2398976531},"base":"PLN","date":"2020-04-30"}
        
3.  **POST** `/currencies/exchange` Umożliwia konwersję walut
Przykłady:
    *  **POST** {"from":"PLN","to": ["USD"],"amount": 420} 
        => {"from":"PLN","currencyValues":[{"name":"USD","rate":0.2398976531,"amount":34.0,"result":8.1565202054,"fee":0.081565202054}]}
    *  **POST** {"from":"PLN","to": ["USD", "EUR"],"amount": 420} 
        => {"from":"PLN","currencyValues":[{"name":"EUR","rate":0.2205752603,"amount":34.0,"result":7.4995588502,"fee":0.074995588502},{"name":"USD","rate":0.2398976531,"amount":34.0,"result":8.1565202054,"fee":0.081565202054}]}

4.  Przykładowe wyjątki:
    *  **GET** `/currencies/PLNN `
        => {"message":"Base 'PLNN' is not supported","httpStatus":"BAD_REQUEST","time":"2020-05-03T00:07:54.1954988+02:00"}
    *  **GET** `/currencies/PLN?filter= `
        => {"message":"Filter parameter can not be empty","httpStatus":"BAD_REQUEST","time":"2020-05-03T00:08:29.0003967+02:00"}