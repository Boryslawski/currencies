package com.mateusz.currencies.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.RestClientResponseException;

import java.time.ZonedDateTime;

@ControllerAdvice
public class ApiExceptionHandler {


    @ExceptionHandler({ApiRequestException.class})
    public ResponseEntity<Object> handleApiRequestException(ApiRequestException e) {
        final HttpStatus status = HttpStatus.BAD_REQUEST;
        final ApiException apiException = ApiException.ApiExceptionBuilder.newBuilder()
                .withMessage(e.getMessage())
                .withHttpStatus(status)
                .withTime(ZonedDateTime.now())
                .build();

        return new ResponseEntity<>(apiException, status);
    }

    @ExceptionHandler({ApiTimeoutException.class})
    public ResponseEntity<Object> handleApiTimeoutException(ApiTimeoutException e) {
        final HttpStatus status = HttpStatus.REQUEST_TIMEOUT;
        final ApiException apiException = ApiException.ApiExceptionBuilder.newBuilder()
                .withMessage(e.getMessage())
                .withHttpStatus(status)
                .withTime(ZonedDateTime.now())
                .build();

        return new ResponseEntity<>(apiException, status);
    }

    @ExceptionHandler({RestClientResponseException.class})
    public ResponseEntity<Object> handleApiExternalException(RestClientResponseException e) {
        final String msg = e.getMessage().substring(e.getMessage().indexOf("error") + 8,
                e.getMessage().indexOf("}", e.getMessage().indexOf("error")) - 2);
        final HttpStatus status = HttpStatus.valueOf(e.getRawStatusCode());
        final ApiException apiException = ApiException.ApiExceptionBuilder.newBuilder()
                .withMessage(msg)
                .withHttpStatus(status)
                .withTime(ZonedDateTime.now())
                .build();

        return new ResponseEntity<>(apiException, status);
    }
}
