package com.mateusz.currencies.Exceptions;

import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;

public class ApiException {

    final private String message;
    final private HttpStatus httpStatus;
    final private ZonedDateTime time;


    public ApiException(String message, HttpStatus httpStatus, ZonedDateTime time) {
        this.message = message;
        this.httpStatus = httpStatus;
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public ZonedDateTime getTime() {
        return time;
    }

    static class ApiExceptionBuilder {

        private String message;
        private HttpStatus httpStatus;
        private ZonedDateTime time;


        static ApiExceptionBuilder newBuilder() {
            return new ApiExceptionBuilder();
        }

        ApiExceptionBuilder withMessage(String msg) {
            this.message = msg;
            return this;
        }


        ApiExceptionBuilder withHttpStatus(HttpStatus httpSt) {
            this.httpStatus = httpSt;
            return this;
        }

        ApiExceptionBuilder withTime(ZonedDateTime ti) {
            this.time = ti;
            return this;
        }

        ApiException build() {
            return new ApiException(message, httpStatus, time);
        }
    }
}


