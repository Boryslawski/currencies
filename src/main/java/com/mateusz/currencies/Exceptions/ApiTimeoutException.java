package com.mateusz.currencies.Exceptions;

public class ApiTimeoutException extends RuntimeException {

    public ApiTimeoutException(String message) {
        super(message);
    }
}
