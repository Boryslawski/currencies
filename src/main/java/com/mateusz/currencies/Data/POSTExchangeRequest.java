package com.mateusz.currencies.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class POSTExchangeRequest implements Serializable {

    private String from;
    private List<String> to;
    private Double amount;


    public POSTExchangeRequest() {
    }
}
