package com.mateusz.currencies.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExchangeData implements Serializable {

    private String from;
    private Set<CurrencyValues> currencyValues = new HashSet<>();


    public ExchangeData(String from) {
        this.from = from;
    }

    public void addValues(CurrencyValues values) {
        this.currencyValues.add(values);
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CurrencyValues {
        private String name;
        private Double rate;
        private Double amount;
        private Double result;
        private Double fee;
    }
}



