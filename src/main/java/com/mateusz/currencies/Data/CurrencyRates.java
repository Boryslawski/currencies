package com.mateusz.currencies.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrencyRates implements Serializable {

    private Map<String, Double> rates;
    private String base;
    private String date;
}
