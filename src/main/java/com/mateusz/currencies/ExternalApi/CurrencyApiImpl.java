package com.mateusz.currencies.ExternalApi;

import com.mateusz.currencies.Data.CurrencyRates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class CurrencyApiImpl implements CurrencyApi {

    @Autowired
    private RestTemplate restTemplate;


    public CurrencyRates getCurrencyRates(@NonNull String source, @Nullable List<String> targets) throws RestClientResponseException {
        final String request = ExchangeApiRequestBuilder.buildGetCurrencyRequest(source, targets);
        return restTemplate.getForObject(request, CurrencyRates.class);
    }
}
