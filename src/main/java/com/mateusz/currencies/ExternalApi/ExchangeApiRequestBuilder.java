package com.mateusz.currencies.ExternalApi;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


class ExchangeApiRequestBuilder {

    private static final String API_ADDRESS = "https://api.exchangeratesapi.io/";
    private static final String API_GET_METHOD = "latest?";
    private static final String SOURCE = "base=%s";
    private static final String REQUIRED_REQUEST_TEMPLATE =
            API_ADDRESS + API_GET_METHOD + SOURCE;

    private static final String TARGET = "&symbols=";
    private static final String TARGETS_DELIMITER = ",";
    private static final String EMPTY_STRING = "";


    public static String buildGetCurrencyRequest(@NonNull String source, @Nullable List<String> targets) {
        return String.format(REQUIRED_REQUEST_TEMPLATE, source)
                + fillSourceTemplate(targets);
    }

    private static String fillSourceTemplate(List<String> targets) {
        return Objects.nonNull(targets)
                ? TARGET + unpackTargetsArray(targets)
                : EMPTY_STRING;
    }

    private static String unpackTargetsArray(List<String> targets) {
        return targets.stream()
                .map(Object::toString)
                .collect(Collectors.joining(TARGETS_DELIMITER));
    }
}
