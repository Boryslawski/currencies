package com.mateusz.currencies.ExternalApi;

import com.mateusz.currencies.Data.CurrencyRates;
import org.springframework.web.client.RestClientResponseException;

import java.util.List;

public interface CurrencyApi {

    CurrencyRates getCurrencyRates(String source, List<String> targets) throws RestClientResponseException;
}

