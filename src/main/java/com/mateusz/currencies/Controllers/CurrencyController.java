package com.mateusz.currencies.Controllers;

import com.mateusz.currencies.Data.*;
import com.mateusz.currencies.Exceptions.ApiRequestException;
import com.mateusz.currencies.Services.ExchangeApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

@RestController
public class CurrencyController {

    private static final String EMPTY_VALUE = "";

    @Autowired
    private ExchangeApiService service;


    @GetMapping("/currencies/{source}")
    public CurrencyRates currencies(
            @PathVariable String source,
            @RequestParam(value = "filter", required = false) List<String> target) {

        if (Objects.nonNull(target) && (target.isEmpty() || target.contains(EMPTY_VALUE)))
            throw new ApiRequestException("Filter parameter can not be empty");

        return service.getCurrencyRates(source, target);
    }

    @PostMapping(value = "/currencies/exchange")
    public ExchangeData exchange(
            @RequestBody POSTExchangeRequest request) {
        try {
            return service.exchange(request.getFrom(), request.getTo(), request.getAmount()).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new ApiRequestException("Request took to long");
        }
    }
}
