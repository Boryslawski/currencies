package com.mateusz.currencies.Services;

import com.mateusz.currencies.Data.CurrencyRates;
import com.mateusz.currencies.Data.ExchangeData;
import com.mateusz.currencies.ExternalApi.CurrencyApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;


@Service
public class ExchangeServiceImpl implements ExchangeApiService {

    private static final Double COMMISSION = 0.01;

    @Autowired
    private CurrencyApi api;


    @Override
    public CurrencyRates getCurrencyRates(@NonNull String source, @Nullable List<String> targets) {
        return api.getCurrencyRates(source, targets);
    }

    @Override
    @Async
    public CompletableFuture<ExchangeData> exchange(@NonNull String from, @NonNull List<String> tos, Double amount) {
        CurrencyRates rates = api.getCurrencyRates(from, tos);

        ExchangeData result = new ExchangeData(from);
        rates.getRates()
                .entrySet()
                .forEach(rate -> result.addValues(calculate(rate, amount)));
        return CompletableFuture.completedFuture(result);
    }

    private ExchangeData.CurrencyValues calculate(Map.Entry<String, Double> rate, Double amount) {
        ExchangeData.CurrencyValues values = new ExchangeData.CurrencyValues();
        values.setName(rate.getKey());
        values.setRate(rate.getValue());
        values.setAmount(amount);
        values.setResult(rate.getValue() * amount);
        values.setFee(rate.getValue() * amount * COMMISSION);
        return values;
    }
}
