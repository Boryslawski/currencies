package com.mateusz.currencies.Services;


import com.mateusz.currencies.Data.CurrencyRates;
import com.mateusz.currencies.Data.ExchangeData;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface ExchangeApiService {

    CurrencyRates getCurrencyRates(@NonNull String source, @Nullable List<String> targets);

    CompletableFuture<ExchangeData> exchange(@NonNull String from, @NonNull List<String> tos, @NonNull Double amount);
}
