package com.mateusz.currencies.TestSupport;

import com.mateusz.currencies.Data.CurrencyRates;
import com.mateusz.currencies.Data.ExchangeData;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;


public class TestSupport {

    public static ExchangeData createExchangeDataMockData(String base, Double amount, Double commission, RatePair... ratePairs) {
        ExchangeData result = new ExchangeData(base);

        Stream.of(ratePairs).forEach(rate -> {
            ExchangeData.CurrencyValues value = new ExchangeData.CurrencyValues();
            value.setName(rate.getName());
            value.setRate(rate.getRate());
            value.setAmount(amount);
            value.setResult(rate.getRate() * amount);
            value.setFee(rate.getRate() * amount * commission);
            result.addValues(value);
        });

        return result;
    }

    public static CurrencyRates createCurrencyRatesMockData(String base, TestSupport.RatePair... ratePairs) {
        CurrencyRates result = new CurrencyRates();

        Map<String, Double> rates = new HashMap<>();
        Stream.of(ratePairs)
                .forEach(pair -> rates.put(pair.getName(), pair.getRate()));

        result.setRates(rates);
        result.setBase(base);
        return result;
    }

    @Data
    public static class RatePair {
        private String name;
        private Double rate;

        public RatePair(String name, Double rate) {
            this.name = name;
            this.rate = rate;
        }
    }
}
