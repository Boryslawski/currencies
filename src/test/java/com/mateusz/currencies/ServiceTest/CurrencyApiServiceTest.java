package com.mateusz.currencies.ServiceTest;


import com.mateusz.currencies.Data.CurrencyRates;
import com.mateusz.currencies.Data.ExchangeData;
import com.mateusz.currencies.ExternalApi.CurrencyApi;
import com.mateusz.currencies.Services.ExchangeApiService;
import com.mateusz.currencies.TestSupport.TestSupport;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.*;


@SpringBootTest
public class CurrencyApiServiceTest {

    @MockBean
    private CurrencyApi currencyApi;

    @Autowired
    private ExchangeApiService exchangeApiService;


    @Before
    public void setUp() {
        Mockito.reset(currencyApi);
    }

    @SneakyThrows
    @Test
    void is_exchange_correct() {
        final String TEST_BASE = "PLN";
        final String TEST_CURRENCY_USD = "USD";
        final Double TEST_CURRENCY_RATES_USD = 0.33;
        final String TEST_CURRENCY_EUR = "EUR";
        final Double TEST_CURRENCY_RATES_EUR = 0.22;

        final Double AMOUNT = 420d;
        final Double COMMISSION = 0.01;

        TestSupport.RatePair[] testDataPairs = {new TestSupport.RatePair(TEST_CURRENCY_USD, TEST_CURRENCY_RATES_USD),
                new TestSupport.RatePair(TEST_CURRENCY_EUR, TEST_CURRENCY_RATES_EUR)};

        // Prepare mock data
        CurrencyRates testData = TestSupport.createCurrencyRatesMockData(TEST_BASE, testDataPairs);
        Mockito.when(currencyApi.getCurrencyRates(Mockito.anyString(), Mockito.anyList())).thenReturn(testData);

        // call test service
        ExchangeData result = exchangeApiService.exchange(TEST_BASE,
                new ArrayList<>(testData.getRates().keySet()),
                AMOUNT).get();

        // Prepare expected data
        ExchangeData expectation = TestSupport.createExchangeDataMockData(TEST_BASE, AMOUNT, COMMISSION, testDataPairs);

        Assert.assertEquals(result.getCurrencyValues(), expectation.getCurrencyValues());
    }
}
