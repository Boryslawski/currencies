package com.mateusz.currencies.ControllerTest;

import com.mateusz.currencies.Services.ExchangeApiService;
import com.mateusz.currencies.TestSupport.TestSupport;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.concurrent.CompletableFuture;


@SpringBootTest
@EnableWebMvc
@AutoConfigureMockMvc
public class CurrencyControllerTest {

    @MockBean
    private ExchangeApiService service;

    @Autowired
    private MockMvc mockMvc;


    @Test
    void should_return_currency_rate() throws Exception {
        final String TEST_BASE = "PLN";
        final String TEST_CURRENCY_USD = "USD";
        final Double TEST_CURRENCY_RATES_USD = 0.33;

        Mockito.when(service.getCurrencyRates(Mockito.any(), Mockito.any()))
                .thenReturn(TestSupport.createCurrencyRatesMockData(
                        TEST_BASE,
                        new TestSupport.RatePair(TEST_CURRENCY_USD, TEST_CURRENCY_RATES_USD)
                ));

        mockMvc.perform(MockMvcRequestBuilders.get("/currencies/PLN")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("base").value(TEST_BASE))
                .andExpect(MockMvcResultMatchers.jsonPath("rates").isMap())
        ;

    }

    @Test
    void should_return_exchange() throws Exception {
        final String TEST_BASE = "PLN";
        final Double AMOUNT = 420d;
        final Double COMMISSION = 0.01;
        final String TEST_CURRENCY_USD = "USD";
        final Double TEST_CURRENCY_RATES_USD = 0.33;

        Mockito.when(service.exchange(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(CompletableFuture.completedFuture(
                        TestSupport.createExchangeDataMockData(
                                TEST_BASE, AMOUNT, COMMISSION,
                                new TestSupport.RatePair(TEST_CURRENCY_USD, TEST_CURRENCY_RATES_USD)
                        )
                ));

        mockMvc.perform(MockMvcRequestBuilders.post("/currencies/exchange")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"from\": \"PLN\",\"to\": [\"USD\"],\"amount\": 420}"))
                .andExpect(MockMvcResultMatchers.status().isOk())
        ;
    }
}
